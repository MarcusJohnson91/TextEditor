#include "../include/TextEditor.hxx"

int main(int argc, char *argv[]) {
    QApplication TextEditApp(argc, argv);
    
    TextEditor TextEdit;
    TextEdit.show();
    
    return TextEditApp.exec();
}

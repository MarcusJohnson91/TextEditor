#include "../include/TextEditor.hxx"
#include "ui_TextEditor.h"

TextEditor::TextEditor(QWidget *Parent) : QMainWindow(Parent), UI(new Ui::TextEditor) {
    UI->setupUi(this);
    this->setCentralWidget(UI->textEdit);
}

TextEditor::~TextEditor() {
    delete UI;
}

void TextEditor::on_actionOpen_triggered() {
    QString InputFile = QFileDialog::getOpenFileName(this, "Which file do you want to open?");
    QFile File(InputFile);
    Document = InputFile;
    if (!File.open(QIODevice::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, "..", "File not opened");
        return;
    } else {
        QTextStream in(&File);
        QString Text = in.readAll();
        UI->textEdit->setText(Text);
        File.close();
    }
}

void TextEditor::on_actionClear_triggered() {
    Document = "";
    UI->textEdit->setText("");
}

void TextEditor::on_actionCopy_triggered() {
    Document = "";
    UI->textEdit->setText("");
}

void TextEditor::on_actionPaste_triggered() {
    Document = "";
    UI->textEdit->setText("");
}

void TextEditor::on_actionSave_triggered() {
    QFile File(Document);
    if (!File.open(QIODevice::ReadWrite | QFile::Text)) {
        // If the file isn't open, we should just ask for where to save it...
        QString File2Save = QFileDialog::getSaveFileName(this, "Which file do you want to overwrite?");
        QFile File(File2Save);
        Document = File2Save;
    } else {
        QFile NewFile(Document);
        QTextStream OutputFile(&NewFile);
        QString PlainText = UI->textEdit->toPlainText();
        OutputFile << PlainText;
        File.flush();
        File.close();
    }
}

void TextEditor::on_actionSaveAs_triggered() {
    // Basically both Open, and Save.
    QString OutputFile = QFileDialog::getSaveFileName(this, "Which file do you want to overwrite?", ".txt");
    QFile File(OutputFile);
    Document = OutputFile;
    if (!File.open(QIODevice::ReadWrite | QFile::WriteOnly)) {
        QMessageBox::warning(this, "..", "Couldn't open File");
    } else {
        QTextStream Out(&File);
        QString PlainText = UI->textEdit->toPlainText();
        Out << PlainText;
        File.flush();
        File.close();
    }
}

void TextEditor::on_actionQuit_triggered() {
    std::exit(-1);
}

#include <fstream>
#include <QApplication>
#include <QFile>
#include <QFileDialog>
#include <QFont>
#include <QFontDialog>
#include <QMainWindow>
#include <QMessageBox>
#include <QTextStream>
#include <QGuiApplication>

#ifndef TextEdit_HXX
#define TextEdit_HXX

namespace Ui {
    class TextEditor;
}

class TextEditor : public QMainWindow {
    Q_OBJECT
public:
    explicit TextEditor(QWidget *Parent = nullptr);
    ~TextEditor();
    
    private slots:
    void on_actionOpen_triggered();

    void on_actionClear_triggered();

    void on_actionCopy_triggered();

    void on_actionPaste_triggered();
    
    void on_actionSave_triggered();
    
    void on_actionSaveAs_triggered();
    
    void on_actionQuit_triggered();
    
private:
    Ui::TextEditor *UI;
    QString         Document;
};


#endif // TextEdit_HXX
